projectL readme_e.txt
ver.1000
HIZ
inspired by projectN(D.K)

Slash & slash & slash! sword action shooting.

*** control

- move ship    　　　    　　　arrow key,ten key, [WASD] / joystick
- change to laser      　　　[Z][left Ctrl][.]             / Trigger 1, 4, 7

in title
- sound ON/OFF　　　　　　　　[X][left Alt][left Shift][/]     / Trigger 2, 6, 8
- toggle title/ranking　　	left and right


- toggle window/fullscreen  F9 key
- hide window     　　　　　 F5 key
- soft reset     　　　　　　 ENTER key
- quit　　　　　　　　　　 ESC key 

*** rule

Laser and sword are rotate to opposite direction of ship moving.

Laser can slash green bullets.
Sword can slash enemies.
When you use sword, game speed is slow
and when you use laser, game speed is high.

When you kill off red enemies, next enemie group is appeared. 
There is no collision detection in wireframe.

*** Comments

If you have any comments, please mail to hiz_oka@yahoo.co.jp

*** Acknowledgement

ProjectL is inspired by projectN produced by D.K(http://www5b.biglobe.ne.jp/~ms06j/main1.htm)
ProjectL is written in the D Programming Language(ver. 0.175).
Simple DirectMedia Layer is used for media handling.
SDL_mixer and Ogg Vorbis CODEC are used for playing BGMs/SEs.
D Header files at D - porting are for use with OpenGL, SDL and SDL_mixer.
Mersenne Twister is used for creating a random number.
ABA's game sources are refered.
kenmo's introduction to D program language (http://www5.atwiki.jp/yaruhara/pages/74.html) are refered.

sound materials

-musics
B.P.M(http://bpm.cside.com/)
FLESH&BONE(http://www.fleshbone.com/index.html)

-sound effects
The･matchmakers(http://osabisi.sakura.ne.jp/m2/)
Hanac200x(http://homepage1.nifty.com/hp_tk_prg/index.html)

-voices
B.P.M(http://bpm.cside.com/)


*** Change log
2006/01/28 ver1.001
・corrected the behavior of sword and laser.
2006/12/22 ver1.000
・released.
