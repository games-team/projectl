module br.shapeImpl;
private import util.shape;
public class BaseOfWing{
	private{
		static Shape shape;
		static  float[][] a = 
		[
    	    [-0.2 ,0 ,0.2   ],
					[-0.2 ,0 ,0.2 ]
	
	     ];
	  static float[][] b =
		[
	       [0 ,0.5 ,0   ],
				 [0 ,1.0 ,0 ]
	       
	     ];
	}
	
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
	
}

public class Wing{
	private{
		static Shape shape;
		static  float[][] a = 
		[
    	    [-1.0 ,-0.5 ,1.0  ],
	
	     ];
	  static float[][] b =
		[
	       [0 ,0.5 ,0   ]
	       
	     ];
	}
	
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class Tail{
	private{
		static Shape shape;
		static  float[][] a = 
		[
    	    [-1.0 ,-0.5 ,1.0  ],
	
	     ];
	  static float[][] b =
		[
	       [0 ,0.5 ,0   ]
	       
	     ];
	}
	
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class Head{
	private{
		static Shape shape;
		static  float[] a = 
    	    [-1.0 ,-0.5 ,1.0 ,-0.5 ]
					;
	  static float[] b =
	       [0 ,0.5 ,0 ,-0.5   ]
	       ;
		static float[] z =
					[-0.5 ,0 ,0.5];
		static float[] scale =
					[0.3 ,1.0 ,0.3];
	}
	
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return  cast(Shape)shape.clone();
		
	}
}
public class MyShip{
		private{
		static Shape shape;
		static  float[] a = 
    	    [-1.5 ,-0.75 ,1.5 ,-0.75 ]
					;
	  static float[] b =
	       [0 ,0.75 ,0 ,-0.75   ]
	       ;
		static float[] z =
					[-0.75 ,0 ,0.75];
		static float[] scale =
					[0.0 ,1.0 ,0.0];
	}
	
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class MyShipAppend{
		private{
		static Shape shape;
		static  float[] a = 
    	    [-1.0 ,-0.25 ,1.25 ]
					;
	  static float[] b =
	       [-0.75 ,0.75 ,-0.25 ]
	       ;
		static float[] z =
					[ ,0 ,0.75];
		static float[] scale =
					[1.0 ,0.0];
	}
	
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class TurretShape1{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.0 ,0.5 ,2.0 ]
				;
	 static float[] b =
	      [0.0 	,0.8 	,0.6 ,0.0 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class TurretShape2{
	private:
	static Shape shape;
	static  float[] a = 
	[-1.0 ,-0.3 ,0.8 ,1.5,0.8,-0.3]
   	    
				;
	 static float[] b =
	      [0.0 ,-0.7 ,-0.5 ,0.0 ,0.5 ,0.7]
	      ;
	static float[] z = 
	[-0.6 ,0.0 ,0.6];
	static float[] scale = 
	[0.7 ,1.0 ,0.7];
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class TurretShape3{
	private:
	static Shape shape;
	static  float[] a = 
			[-1.0 ,-0.6 ,0.0 ,0.6 ,0.6 ,2.0 ,2.1 ,2.0 ,1.0 ,2.0 ,2.1 ,2.0 ,0.6 ,0.6 ,0.0 ,-0.6];
  	static float[] b =
		
			[0.0 ,0.6  ,1.0 ,0.6 ,0.6  ,0.5 ,0.3 ,0.1 ,0.0 ,-0.1,-0.3,-0.5,-0.6,-0.6,-1.0,-0.6];
	static float[] z=
	[-0.6 ,-0.4 ,0.0 ,0.4 ,0.6];
	static float[] scale =
	[0.2 ,0.6 ,1.0 ,0.6 ,0.2];
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class NoseShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.0 ,-0.3 ,0.8 ,1.5,0.8,-0.3]
   	    
				;
	 static float[] b =
	      [0.0 ,-0.7 ,-0.5 ,0.0 ,0.5 ,0.7]
	      ;
	static float[] z = 
	[-0.6 ,0.0 ,0.6];
	static float[] scale = 
	[0.7 ,1.0 ,0.7];
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class ShipShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.0 ,0.0 ,1.0 ,1.5 ,0.8 ,0.0 ,-0.8]
				;
	 static float[] b =
	      [0.1 	,0.6 ,0.7,0.6 ,0.1 ,-0.7 ,-0.9 ,-0.7]
	      ;
	static float[] z = 
	[-0.2 ,0.0 ,0.2];
	static float[] scale = 
	[0.7 ,1.0 ,0.7];
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z ,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class Octahedron{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.0 ,0.0 ,1.0]
				;
	 static float[] b =
	      [0.0 	,1.0 	,0.0 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}
public class BulletShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.0 ,0.0 ,2.0]
				;
	 static float[] b =
	      [0.0 	,1.0 	,0.0 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class TubeShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.0 ,1.0 ,1.5]
				;
	 static float[] b =
	      [0.3 	,0.6 ,0.6 ,0.3 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class ArmShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.2 ,0.7 ,1.5]
				;
	 static float[] b =
	      [0.3 	,0.6 ,0.5 ,0.2 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class ArmShape2{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.55 ,-1.5 ,-0.8 ,0.6 ,1.5 ,1.55]
				;
	 static float[] b =
	      [0.0 ,0.4 	,0.6 ,0.55 ,0.45 ,0.0]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,6);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class JointShape{
	
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.4 ,-1.2 ,-0.8 ,0.8 ,1.2 ,1.4 ]
				;
	 static float[] b =
	    [0.0 ,0.6 ,1.0 ,0.8 , 0.6 ,0.0]
	      ;
		  /*
	static float[] z = 
	[-0.4 ,0.0 ,0.4 ,0.7 ];
	static float[] scale = 
	[0.7 ,1.0 ,0.8 ,0.3];
	*/
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,6);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
	
	
}
public class NailShape2{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.6 ,-1.4 , 0.5 ,3.0 ]
				;
	 static float[] b =
	    [0.0 ,0.6 ,0.35 ,0.0]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,6);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class NailShape{
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.6 ,-1.2 ,3.0 ]
				;
	 static float[] b =
	      [0.3 	,0.6 ,0.0 ]
	      ;
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,4);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
}

public class BodyShape{
	
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.5 ,-1.3,-0.7 ,0.5 ,0.9 ,1.0]
				;
	 static float[] b =
	    [0.0  ,0.6 ,1.0  ,0.7 ,0.5,0.0]
	      ;
		  /*
	static float[] z = 
	[-0.2 ,0.0 ,0.2];
	static float[] scale = 
	[0.7 ,1.0 ,0.7];
	*/
	protected static void setShape(){
		shape = new SH_Pole(a ,b ,6);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
	
}

public class Head2{
	
	private:
	static Shape shape;
	static  float[] a = 
   	    [-1.3 ,-1.1 ,-0.6 ,0.5 ,0.8 ,1.1 ,0.9 ,0.0 ,-1.0]
				;
	 static float[] b =
	    [0.0  ,0.6  ,1.0  ,0.6 ,0.4 ,-0.3 ,-0.4 ,-0.2 ,-0.2]
	      ;
		  
	static float[] z = 
	[-0.6 ,0.0 ,0.6 ];
	static float[] scale = 
	[0.5 ,1.0 ,0.5];
	
	protected static void setShape(){
		shape = new SH_Pot(a ,b ,z,scale);
	}
	public static Shape getShape(){
		if(shape is null)setShape();
		return cast(Shape)shape.clone();
		
	}
	
	
}