module br.gamemanager;

private import opengl;
private import SDL;
private import SDL_mixer;
private import std.string;
private import std.math;
private import util.key;
private import util.vector;
private import util.matrix;
private import util.rand;
private import util.collision;
private import util.animation;
private import util.parts;
private import util.particle;
private import util.timer;
private import util.log;
private import util.record;
private import util.ascii;
private import br.append;
private import br.ship;
private import br.blast;
private import br.mainloop;
private import br.enemy;
private import br.screen;
private import br.stage;
private import br.background;
private import br.sound;
private import br.prefmanager;






public Ship ship;

public Rand rand;
public Screen screen;
//public Blast blast;
public CollisionManager collisionManager;
public PartsManager shipManager;
public PartsManager enemyManager;
public PartsManager bulletManager;
public PartsManager ringManager;
//public PartsManager appendManager;
public BackGround background;


private const int MAXSHIP = 10;
private const int DEFAULTSTAGE = 1;
private GameManager gameManager;
private GameState inGameState;
private GameState inResultState;
private GameState inMissState;
private GameState inTitleState;
private GameState inEnding1State;
private GameState inEnding2State;
private int stageNum;
private Stage stage;
private int totalTime;
private int lapTime;
private int restShip;

public bool pressingA;

public class GameManager{
	
	public:
	const int SCREEN_WIDTH = 640;
	const int SCREEN_HEIGHT = 480;
	const int SCREEN_BPP = 0;

	/*
	static int g_videoFlags = SDL_SWSURFACE|SDL_OPENGL;
//	const SDL_VideoInfo* info = NULL;
	SDL_Surface *gScreenSurface;
	*/
	
	//int screen_width;
	//int screen_height;
	//int screen_bpp;
	
	static int count;
	private:
	GameState state;
	GameState nextState;
	
	//Parts[10] append;
	
	Key key;
	
	
	public this(Key key){
		this.key = key;
		gameManager = this;
	}
	public void start(){
		rand = new Rand();
		screen = new Screen();
//		blast = new Blast();
		partsManager = new PartsManager(512);
		shipManager = new PartsManager(10);
		enemyManager = new PartsManager(128);
		bulletManager = new PartsManager(512);
		ringManager = new PartsManager(64);
//		appendManager = new PartsManager(128);
		
		blastManager = new BlastManager(128);
		particleManager = new ParticleManager(2048);
//		animationManager = new AnimationManager(64);
		collisionManager = new CollisionManager(1024);
		collisionManager.clear();
		ship = new Ship(key,0 ,0);//screen.width / 2,screen.height / 2);
		background = new BackGround();
		
		
//		record = new Record(Stage.MAXSTAGE ,3);
		
		if(prefManager.prefData.fullScreen){
			screen.toggleFullScreen();
		}
		nosound(prefManager.prefData.nosound);
		
		
		count = 0;
		totalTime = 0;
		lapTime = 0;
		
		
		Sound_init();
		/*
		Mix_OpenAudio(22050,AUDIO_S16,2,4096);
		
		chunk_slash = Mix_LoadWAV("se/hit_s02.wav");
		chunk_defend = Mix_LoadWAV("se/metal34_a.wav");
		chunk_bomb = Mix_LoadWAV("se/bom13_c.wav");
		chunk_beep = Mix_LoadWAV("se/beep00.wav");
		chunk_warning = Mix_LoadWAV("se/emergency.wav");
		chunk_reverse = Mix_LoadWAV("se/hit_s03_a.wav");
		
		
		if(Mix_OpenAudio(22050,AUDIO_S16,2,4096) < 0)return ;
		music1 = Mix_LoadMUS("music/FREQ_loop005.ogg");
		music2 = Mix_LoadMUS("music/FREQ_loop004.ogg");
		*/
		
		inGameState = new InGameState();
		inResultState = new InResultState();
		inMissState = new InMissState();
		inTitleState = new InTitleState();
		inEnding1State = new InEnding1State();
		inEnding2State = new InEnding2State();
		state = inTitleState;
		state.start();
		
		nextState = null;
		
		pressingA = true;
		
		
		/*
		if (Mix_PlayMusic(music, -1) != 0) {
			throw new Error("Couldn't play music");
		}
		*/
	}
	public void move(){
		
		state.move();
		if(nextState !is null){
			state = nextState;
			
			state.start();
			nextState = null;
		}
		count ++;
		
	}
	public void returnPushed(){
		
		setNextState("titleState");
		
	}
	public void setNextState(char[] name){
		switch(name){
				case "gameState":nextState = inGameState;break;
				case "resultState":nextState = inResultState;break;
				case "missState":nextState = inMissState;break;
				case "titleState":nextState = inTitleState;break;
				case "ending1State":nextState = inEnding1State;break;
				case "ending2State":nextState = inEnding2State;break;
				default:break;
		}
	}
	public void draw(){
//		glClear(GL_DEPTH_BUFFER_BIT);
		state.draw();
	}
	public void clear(){
		shipManager.clear();
		enemyManager.clear();
		bulletManager.clear();
		ringManager.clear();
		partsManager.clear();
//		animationManager.clear();
		shipManager.clear();
		enemyManager.clear();
		bulletManager.clear();
		collisionManager.clear();
	}
	public void close(){
		clear();
		Sound_free();
		Mix_CloseAudio();
	}

}
private bool setStage(int num){
	bool game = true;	
	
	stageNum = 1;
	
	if(num <= Stage.MAXSTAGE)stageNum = num;
	else{
		 stageNum = 9;
		 game = false;
	}
	
	stage = getStage(stageNum);
	stage.goback();
	
	if(stage !is null)Sound_PlayMusic(stage.music);
	
	return game;
}
private Stage getStage(int num){
	
	Stage stage;
	switch(num){
		case 1:stage = new Stage1();break;
		case 2:stage = new Stage2();break;
		case 3:stage = new Stage3();break;
		case 4:stage = new Stage4();break;
		case 5:stage = new Stage5();break;
		case 6:stage = new Stage6();break;
		case 7:stage = new Stage7();break;
		case 8:stage = new Stage8();break;
		case 9:stage = new Ending();break;
		default:stage = null;
	}
	return stage;
}
public abstract class GameState{
	protected:
//	bool _end;
	public void start();
	public void move();
	public void draw();
//	public bool end();
	public char[] name();
}
public class InGameState:GameState{
	private:

	int count;

	
	const char[] _name = "gameState";
	public this(){
	}
	
	public void start(){
		
		count = 0;
		
		
//		timer = 0;
		
//		_end = false;
	}
	public void move(){
		collisionManager.collisionDetect();
		if(stage.cleared){
//			stageNum ++;
			gameManager.setNextState("resultState");
//			_end = true;
			
		}
		
		
		
		if((cast(Ship)ship).slow){
			if(count % 2==0){
				blastManager.run();
				partsManager.move();
				/*
				shipManager.move();
				enemyManager.move();
				bulletManager.move();
				ringManager.move();
				*/
//				appendManager.move();
				
//				animationManager.run();
				stage.run();
				background.move();
				
				particleManager.run();
//				timer++;
			}else shipManager.move();
			
		}else{
			blastManager.run();
			partsManager.move();
			
//			shipManager.move();
			/*
				enemyManager.move();
				bulletManager.move();
				ringManager.move();
//				appendManager.move();
			*/
				
//			animationManager.run();
			stage.run();
			background.move();
			
			particleManager.run();
//			timer++;
		}
		
		if(ship.miss){
			gameManager.setNextState("missState");
			
			ship.miss = false;
		}
		
		count ++;
		lapTime ++;
		totalTime ++;
	}
	public void draw(){
		
		screen.setProjection();
		glPushMatrix();
		background.draw();
		partsManager.draw();
		
		/*
		shipManager.draw();
				enemyManager.draw();
				bulletManager.draw();
				ringManager.draw();
//				appendManager.draw();
		*/
		particleManager.draw();
		blastManager.draw();
//		if(blast.exists)blast.draw();
		screen.setModelView();
		asciiR = 1.0;asciiG =0.88;asciiB = 0.7;
		drawTimer(lapTime ,60 ,-268 ,0.6);
		drawTimer(totalTime ,200 ,-260);
		stage.draw();
//		drawString("ready to destroy the warp hole" ,-300 ,-100 ,5 ,10);
		
		/*
		drawAlphabet('n' ,-350 ,50 );
		drawAlphabet('e' ,-320 ,50 );
		drawAlphabet('w' ,-290 ,50 );
		*/	
		/*
		drawAlphabet('p' ,0 ,20 ,1.0);
		drawAlphabet('r' ,30 ,20 ,1.0);
		drawAlphabet('o' ,60 ,20 ,1.0);_________
		drawAlphabet('j' ,90 ,20 ,1.0);
		drawAlphabet('e' ,120 ,20 ,1.0);
		drawAlphabet('c' ,150 ,20 ,1.0);
		drawAlphabet('t' ,180 ,20 ,1.0);
		drawAlphabet('l' ,220 ,20 ,1.0);
		*/
//		drawNumber((count / 60) % 10 ,300 ,-260);
//		drawColon(320 ,-260);
		glPopMatrix();
	}
/*	
	public bool end(){
		return _end;
	}
	*/
	public char[] name(){
		return _name;
	}
}

public class InResultState:GameState{
	private:
	int count;
	int lapRank ,totalRank;
	protected const char[] _name = "resultState";
	public this(){
		lapRank = -1;
		totalRank = -1;
	}
	public void start(){
		count = 0;
//		_end = false;
		
		lapRank = prefManager.prefData.record.updateLapRecord(stageNum-1 ,lapTime);
		totalRank = prefManager.prefData.record.updateTotalRecord(stageNum-1 ,totalTime);
		Stage next = getStage(stageNum+1);
//		Log_write(stageNum + 1);
//		Log_write(Sound_PlayingMusic);
		
//		if(next !is null)Log_write(next.music);
//			Log_write(next.music);
//			Log_write(Sound_PlayingMusic);
		if(next !is null){
			if(next.music != Sound_PlayingMusic && Sound_PlayingMusic != -1){
				Sound_FadeOutMusic(2000);
			}
		}
	}
	public void move(){
		if(240 < count){
//			_end = true;
			
			
			lapTime = 0;
			
			if(setStage(stageNum + 1))gameManager.setNextState("gameState");
			else gameManager.setNextState("ending1State");
		}
		
		if((cast(Ship)ship).sword){
			if(count % 2==0){
				blastManager.run();
				partsManager.move();
				/*
				shipManager.move();
				enemyManager.move();
				bulletManager.move();
				ringManager.move();
				*/
//				appendManager.move();

				background.move();
				
				particleManager.run();
			}else shipManager.move();
			
		}else{
			/*
			shipManager.move();
			enemyManager.move();
			bulletManager.move();
			ringManager.move();
			*/
//			appendManager.move();
			blastManager.run();
			partsManager.move();
//			stage.run();
			background.move();
			
			particleManager.run();
//			timer++;
		}
		count ++;
	}
	public void draw(){
		screen.setProjection();
		background.draw();
		/*
		shipManager.draw();
		enemyManager.draw();
		bulletManager.draw();
		ringManager.draw();
//		appendManager.draw();
*/
		partsManager.draw();
		particleManager.draw();
		blastManager.draw();
		screen.setModelView();
		/*
		for(int i=0;i<record.lapTime[stageNum-1].length;i++){
			drawTimer(record.lapTime[stageNum-1][i] ,-200 + 200 * i ,-200);
			drawTimer(record.totalTime[stageNum-1][i] ,-200 + 200 * i,-100);
		}
		*/
		
		if(120 < count){
			asciiR = 0.7;asciiG =0.88;asciiB = 1.0;
			drawString("lap" ,-220 ,-50);
			/*
			drawAlphabet('l' ,-220 ,-50 );
			drawAlphabet('a' ,-190 ,-50 );
			drawAlphabet('p' ,-160 ,-50 );
			*/
			drawString("total" ,-250 ,50);
			/*
			drawAlphabet('t' ,-250 ,50 );
			drawAlphabet('o' ,-220 ,50 );
			drawAlphabet('t' ,-190 ,50 );
			drawAlphabet('a' ,-160 ,50 );
			drawAlphabet('l' ,-130 ,50 );
			*/
			
			asciiR = 1.0;asciiG =0.88;asciiB = 0.7;
			drawTimer(lapTime ,-100 ,-50);
			drawTimer(totalTime ,-100 ,50);
			
			if(1 <= lapRank){
				
				switch(lapRank){
					case 1:
					asciiR = 1.0;asciiG =1.00;asciiB = 0.7;
					drawString("st" ,180 ,-60 ,0.6);
					/*
					drawAlphabet('s' ,180 ,-60 ,0.6 );
					drawAlphabet('t' ,200 ,-60 ,0.6);
					*/
					break;
					case 2:
					asciiR = 0.9;asciiG =0.9;asciiB = 0.9;
					drawString("nd" ,180 ,-60 ,0.6);
					/*
					drawAlphabet('n' ,180 ,-60 ,0.6 );
					drawAlphabet('d' ,200 ,-60 ,0.6);
					*/
					break;
					case 3:
					asciiR = 1.0;asciiG =0.8;asciiB = 0.7;
					drawString("rd" ,180 ,-60 ,0.6);
					/*
					drawAlphabet('r' ,180 ,-60 ,0.6 );
					drawAlphabet('d' ,200 ,-60 ,0.6);
					*/
					break;
					default:
					asciiR = 1.0;asciiG =0.8;asciiB = 0.7;
					drawString("th" ,180 ,-60 ,0.6);
					/*
					drawAlphabet('t' ,180 ,-60 ,0.6 );
					drawAlphabet('h' ,200 ,-60 ,0.6);
					*/
					break;
				}
				drawNumber(lapRank ,150 ,-50 ,1.0 + fmax(0.0 ,(150 - count) / 120.0));
			}
			if(1 <= totalRank){
				
				switch(totalRank){
					case 1:
					asciiR = 1.0;asciiG =1.00;asciiB = 0.7;
					drawString("st" ,180 ,40 ,0.6);
					/*
					drawAlphabet('s' ,180 ,40 ,0.6 );
					drawAlphabet('t' ,200 ,40 ,0.6);
					*/
					break;
					case 2:
					asciiR = 0.9;asciiG =0.9;asciiB = 0.9;
					drawString("nd" ,180 ,40 ,0.6);
					/*
					drawAlphabet('n' ,180 ,40 ,0.6 );
					drawAlphabet('d' ,200 ,40 ,0.6);
					*/
					break;
					case 3:
					asciiR = 1.0;asciiG =0.8;asciiB = 0.7;
					drawString("rd" ,180 ,40 ,0.6);
					/*
					drawAlphabet('r' ,180 ,40 ,0.6 );
					drawAlphabet('d' ,200 ,40 ,0.6);
					*/
					break;
					default:
					asciiR = 1.0;asciiG =0.8;asciiB = 0.7;
					drawString("th" ,180 ,40 ,0.6);
					/*
					drawAlphabet('t' ,180 ,40 ,0.6 );
					drawAlphabet('h' ,200 ,40 ,0.6);
					*/
					break;
				}
				drawNumber(totalRank ,150 ,50 ,1.0 + fmax(0.0 ,(150 - count) / 60.0));
			}
			
			/*
			asciiR = 1.0;asciiG =0.7;asciiB = 0.7;
			drawAlphabet('b' ,-150 ,130 );
			drawAlphabet('e' ,-120 ,130 );
			drawAlphabet('s' ,-90 ,130 );
			drawAlphabet('t' ,-60 ,130 );
	
			drawTimer(record.lapTime[stageNum-1][0] , -200 ,-50);
			drawTimer(record.totalTime[stageNum-1][0] ,-200 ,50);
			*/
		}
		asciiR = 1.0;asciiG =0.88;asciiB = 0.7;
		drawTimer(lapTime ,60 ,-268 ,0.6);
		drawTimer(totalTime ,200 ,-260);
	}
	/*
	public bool end(){
		return _end;
	}
	*/
	public char[] name(){
		return _name;
	}
}
public class InMissState:GameState{
	private:
	int count;
	int timer;
	protected const char[] _name = "missState";
	public this(){

	}
	public void start(){
		count = 0;
		timer = 0;
		restShip --;
		
	}
	public void move(){
		if(count == 0){
			if(0 <= restShip){
				Sound_PlaySe(se_kind.V_0 + restShip);
			}else Sound_FadeOutMusic(1000);
		}
		if(count < 30){
			count ++;
		}else {
			if(90 < timer){
				stage.goback();
				shipManager.clear();
				ship = new Ship(gameManager.key ,0 ,0);
				blastManager.clear();
				particleManager.clear();
				enemyManager.clear();
				if(0 <= restShip)gameManager.setNextState("gameState");
				else gameManager.setNextState("titleState");
			}
			int button = gameManager.key.getButtonState();
			if(((button & gameManager.key.Button.A) == 0)){
				pressingA = false;
				if(count % 2==0){
					blastManager.run();
					partsManager.move();
					/*
					shipManager.move();
					enemyManager.move();
					bulletManager.move();
					ringManager.move();
					*/
	//				appendManager.move();
	
					background.move();
					//blastManager.run();
					particleManager.run();
					timer ++;
				}else shipManager.move();
				
			}else{
				pressingA = true;
				/*
				shipManager.move();
				enemyManager.move();
				bulletManager.move();
				ringManager.move();
				*/
	//			appendManager.move();
				blastManager.run();
				partsManager.move();
	//			stage.run();
				background.move();
				
				particleManager.run();

				timer ++;
			}
		}
		count ++;
		
	}
	public void draw(){
		screen.setProjection();
		background.draw();
		
		partsManager.draw();
		particleManager.draw();
		blastManager.draw();
		screen.setModelView();
		
		if(count <75){
			asciiR = 1.0;asciiG =0.88;asciiB = 0.7;asciiAlpha = fmax((90.0 - count) / 60.0 ,0.0);
			if(0 <= restShip){
				
				drawString("rest" ,-80 ,0 ,1.0);
				drawNumber(restShip ,60 ,0 ,1.2);
			}else {
				drawString("gameover" ,-120 ,0 ,1.0);
			}
				asciiAlpha = 1.0;
		}
		asciiR = 1.0;asciiG =0.88;asciiB = 0.7;
		drawTimer(lapTime ,60 ,-268 ,0.6);
		drawTimer(totalTime ,200 ,-260);
	}
	
	public char[] name(){
		return _name;
	}
}
public class InTitleState:GameState{
	private:
	const enum {
		TITLE ,TITLEtoRANKING ,RANKING ,RANKINGtoTITLE
	};
	int count;
	bool pressingB;
	int x,y;
	int state;
	
	protected const char[] _name = "missState";
	public this(){
//		name = "resultState";
		state = TITLE;
		x = 0;
		y = 0;
	}
	public void start(){
		count = 0;
		lapTime = 0;
		totalTime = 0;
		restShip = MAXSHIP;
		Sound_FadeOutMusic(1000);
		pressingB = false;
		
		background.start();
		
	}
	public void move(){
		int button = gameManager.key.getButtonState();
		
		
		if(((button & gameManager.key.Button.B) != 0)){
			
			if(!pressingB){
				nosound = true ^ nosound;
			}
			pressingB = true;
			
		}else pressingB = false;
		if((cast(Ship)ship).slow){
			if(count % 2==0){
				
				background.move();
				
			}
			
		}else{
			
			background.move();
			
		}
		
		stateMove();
		
		count ++;
	}
	public void stateMove(){
		int button = gameManager.key.getButtonState();
		switch(state){
			case TITLE:
			if(((button & gameManager.key.Button.A) != 0)){
				if(!pressingA){
					
					
					//if(ship !is null)ship.vanish();
					
					shipManager.clear();
					ship = new Ship(gameManager.key ,0 ,0);
					blastManager.clear();
					particleManager.clear();
					if(setStage(DEFAULTSTAGE))gameManager.setNextState("gameState");
					else gameManager.setNextState("ending1State");
				}
				pressingA = true;
			}else pressingA = false;
			if(gameManager.key.getDirState() & gameManager.key.Dir.RIGHT){
				state = TITLEtoRANKING;
			}
			break;
			case TITLEtoRANKING:
			if(-1000 < x)x -= 50;
			else{
				x = -1000;
				 state = RANKING;
			}
			if(gameManager.key.getDirState() &gameManager.key. Dir.LEFT){
				state = RANKINGtoTITLE;
			}
			
			break;
			case RANKING:
			if(gameManager.key.getDirState() & gameManager.key.Dir.LEFT){
				state = RANKINGtoTITLE;
			}
			if(gameManager.key.getDirState() &gameManager.key. Dir.UP){
				y -= 5;
			}else if(gameManager.key.getDirState() &gameManager.key. Dir.DOWN){
				y += 5;
			}
			if(y < 0)y = 0;
			if(80 * (Stage.MAXSTAGE - 1) < y)y = 80 * (Stage.MAXSTAGE - 1);
			break;
			case RANKINGtoTITLE:
			if(x < 0)x += 50;
			else {
				x = 0;
				state = TITLE;
			}
			if(gameManager.key.getDirState() & gameManager.key.Dir.RIGHT){
				state = TITLEtoRANKING;
			}
			
			break;
			default:break;
		}
	}
	public void draw(){
		screen.setProjection();
		background.draw();
		
		
		glPushMatrix();
		asciiR = 1.0;asciiG =0.88;asciiB = 0.7;
		glTranslatef(x ,0 ,0);
		
		drawString("project l" ,-120  ,0 ,1.0);
		drawString("start" ,-45  ,-100 ,0.6);
		
		int dy = y;
		glTranslatef(1000 ,0 ,0);
//		drawString("stage" ,-280 
		for(int i=0;i<Stage.MAXSTAGE;i++){
			asciiAlpha = (300.0 - abs(dy)) / 300.0;
			asciiR = 0.8;asciiG =0.8;asciiB = 0.8;
			drawString("stage" ,-380 ,dy ,0.6);
			drawNumber(i+1 ,-290 ,dy ,0.6);
			asciiR = 0.8;asciiG =0.8;asciiB = 0.8;
			drawString("lap" ,-240 ,-20 + dy ,0.6);
			drawString("total" ,-250 ,20 + dy ,0.6);
			asciiR = 1.0;asciiG =0.88;asciiB = 0.7;
			drawTimer(prefManager.prefData.record.lapTime[i][0] , -100 ,-20 + dy ,0.6);
			drawTimer(prefManager.prefData.record.totalTime[i][0] ,-100 ,20 + dy ,0.6);
			asciiR = 0.95;asciiG =0.82;asciiB = 0.63;
			drawTimer(prefManager.prefData.record.lapTime[i][1] , 50 ,-20 + dy ,0.6);
			drawTimer(prefManager.prefData.record.totalTime[i][1] ,50 ,20 + dy ,0.6);
			asciiR = 0.9;asciiG =0.79;asciiB = 0.57;
			drawTimer(prefManager.prefData.record.lapTime[i][2] , 200 ,-20 + dy ,0.6);
			drawTimer(prefManager.prefData.record.totalTime[i][2] ,200 ,20 + dy ,0.6);
			dy -= 80;
		}
		glPopMatrix();
		asciiAlpha =  1.0;
		
		if(nosound){
			asciiR = 0.6;asciiG =0.6;asciiB = 0.6;
			drawString("off" ,350 ,-260 ,0.6);
		}else{
			asciiR = 1.0;asciiG =1.0;asciiB = 1.0;
			drawString("on" ,350 ,-260 ,0.6);
		}
		drawString("sound" ,250 ,-260 ,0.6);
	}
	
	public char[] name(){
		return _name;
	}
}


public class InEnding1State:GameState{
	private:

	int count;

	
	const char[] _name = "ending1State";
	public this(){
	}
	
	public void start(){
		
		count = 0;
		
		
//		timer = 0;
		
//		_end = false;
	}
	public void move(){
//		collisionManager.collisionDetect();
		if(stage.cleared){
//			stageNum ++;
			gameManager.setNextState("ending2State");
//			_end = true;
			
		}
				
		
		if((cast(Ship)ship).slow){
			if(count % 2==0){
				blastManager.run();
				partsManager.move();
				/*
				shipManager.move();
				enemyManager.move();
				bulletManager.move();
				ringManager.move();
				*/
//				appendManager.move();
				
//				animationManager.run();
				stage.run();
				background.move();
				
				particleManager.run();
//				timer++;
			}else shipManager.move();
			
		}else{
			blastManager.run();
			partsManager.move();
			
//			shipManager.move();
			/*
				enemyManager.move();
				bulletManager.move();
				ringManager.move();
//				appendManager.move();
			*/
				
//			animationManager.run();
			stage.run();
			background.move();
			
			particleManager.run();
//			timer++;
		}
		
		
		
		

		count ++;

	}
	public void draw(){
		
		screen.setProjection();
		glPushMatrix();
		background.draw();
		partsManager.draw();
		
		/*
		shipManager.draw();
				enemyManager.draw();
				bulletManager.draw();
				ringManager.draw();
//				appendManager.draw();
		*/
		particleManager.draw();
		blastManager.draw();
//		if(blast.exists)blast.draw();
		screen.setModelView();
		/*
		asciiR = 1.0;asciiG =0.88;asciiB = 0.7;
		drawTimer(lapTime ,60 ,-268 ,0.6);
		drawTimer(totalTime ,200 ,-260);
		*/
		stage.draw();
//		drawString("ready to destroy the warp hole" ,-300 ,-100 ,5 ,10);
		
		/*
		drawAlphabet('n' ,-350 ,50 );
		drawAlphabet('e' ,-320 ,50 );
		drawAlphabet('w' ,-290 ,50 );
		*/	
		/*
		drawAlphabet('p' ,0 ,20 ,1.0);
		drawAlphabet('r' ,30 ,20 ,1.0);
		drawAlphabet('o' ,60 ,20 ,1.0);_________
		drawAlphabet('j' ,90 ,20 ,1.0);
		drawAlphabet('e' ,120 ,20 ,1.0);
		drawAlphabet('c' ,150 ,20 ,1.0);
		drawAlphabet('t' ,180 ,20 ,1.0);
		drawAlphabet('l' ,220 ,20 ,1.0);
		*/
//		drawNumber((count / 60) % 10 ,300 ,-260);
//		drawColon(320 ,-260);
		glPopMatrix();
	}
/*	
	public bool end(){
		return _end;
	}
	*/
	public char[] name(){
		return _name;
	}
}
public class InEnding2State:GameState{
	private:

	int count;

	int timer;
	const char[] _name = "ending2State";
	public this(){
	}
	
	public void start(){
		
		count = 0;
		Sound_PlayMusic(music_kind.MUSIC9);
		
		timer = 0;
		
//		_end = false;
	}
	public void move(){
//		collisionManager.collisionDetect();
		if(timer*2 > 4930){
//			stageNum ++;
			gameManager.setNextState("titleState");
//			_end = true;
			
		}
		
		
		
		int button = gameManager.key.getButtonState();
		if(((button & gameManager.key.Button.A) == 0)){
			pressingA = false;
			if(count % 2==0){
				timer++;
			}
			
		}else{
			pressingA = true;
			timer++;
		}
		
		count ++;
	}
	public void draw(){
		screen.setProjection();
		glPushMatrix();
		screen.setModelView();
		
		drawStaffRoll();
		
		glPopMatrix();
	}
	
	public void drawStaffRoll(){
		asciiR = 1.0;asciiG = 1.0;asciiB = 0.9;asciiAlpha=1.0;
		int y;
		y=timer*2 - 330;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("projectL staff" ,y);
		y=timer*2 - 930;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("all" ,y);
		y=timer*2 - 1030;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("hiz" ,y);
		y=timer*2 - 1630;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("inspired by" ,y);
		y=timer*2 - 1730;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("project N" ,y);
		y=timer*2 - 1790;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("developed by D.K" ,y);
		y=timer*2 - 2390;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("adviced by" ,y);
		y=timer*2 - 2490;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("D.K" ,y);
		y=timer*2 - 2550;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("and gamehell 2000" ,y);
		y=timer*2 - 3150;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("sound materials" ,y);
		y=timer*2 - 3250;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("B.P.M" ,y);
		y=timer*2 - 3310;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("FLESH AND BONE" ,y);
		y=timer*2 - 3370;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("THE MATCHMAKERS" ,y);
		y=timer*2 - 3430;asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(-330 < y && y < 330)drawStringCenter("Hanac200x" ,y);
		y=cast(int)fmin(0.0 ,timer*2.0 - 4030.0);asciiAlpha = (300.0 - abs(y)) / 300.0;
		if(timer*2 > 4810)asciiAlpha = 1.0 -(timer*2.0 -4810.0) /120.0;
		if(-330 < y && y < 330)drawStringCenter("THANKS FOR YOUR PLAYING" ,y);
	}

	public char[] name(){
		return _name;
	}
}
