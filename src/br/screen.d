module br.screen;
private import opengl;
private import SDL;
private import std.string;
private import std.c.stdlib;
private import br.mainloop;


public class Screen{
	public:
	const int SCREEN_WIDTH = 640;
	const int SCREEN_HEIGHT = 480;
	const int SCREEN_BPP = 0;

	const int GAME_LEFT = -400;
	const int GAME_RIGHT = 400;
	const int GAME_UP = 300;
	const int GAME_DOWN = -300;
	const int GAME_NEAR = -2;
	const int GAME_FAR = -1600;

	static int g_videoFlags = SDL_SWSURFACE|SDL_OPENGL;
	SDL_Surface *gScreenSurface;
	int width;
	int height;
	int bpp;
	public this(){
//		setenv("SDL_VIDEODRIVER","directx" ,1);
//		setenv("SDL_AUDIODRIVER","directx" ,1);
		
		if(SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0 ) {
			throw new Exception(
        "Unable to init SDL video: " ~ std.string.toString(SDL_GetError()));
		}
		//info = SDL_GetVideoInfo( );
		
		width = 	SCREEN_WIDTH;
		height = SCREEN_HEIGHT;
		bpp = SCREEN_BPP;
		SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	//	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
		gScreenSurface = SDL_SetVideoMode(
		width,
		height,
		bpp,
		g_videoFlags
		);
		SDL_WM_SetCaption("projectL", null);
		
		
		//glFrustum(0 ,width ,height ,0 ,1 ,400);
		
		//(width - height) / 2 ,0 ,height ,height);
		glClearColor(0 ,0 ,0,1.0);
//		glEnable(GL_DEPTH_TEST);
//		glEnable(GL_BLEND);
//		glEnable(GL_LINE_SMOOTH);
//		glEnable(GL_POLYGON_SMOOTH);
		
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glFrustum(0 , 4 , 2 , 0 , 2 , 10);
		
//		glClearColor(0.1 ,0.08 ,0 ,1.0);
		
	}
	public void setProjection(){
		glLoadIdentity();
		glMatrixMode(GL_PROJECTION);
		
		glFrustum(-1.0 , 1.0 , -0.75 , 0.75 , 2 , 1600);
		glViewport(0 ,0 ,width ,height);
	}
	public void setModelView(){
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		
		glOrtho(-400, 400, -300, 300, 2, 1600);
		glViewport(0 ,0 ,width ,height);
	}
	public void setClearColor(double R ,double G,double B,double alpha){
		glClearColor(R ,G ,B ,alpha);
	}
	public void clear() {
    glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);
  }
	public void flip() {
    //handleError();
    SDL_GL_SwapBuffers();
  }
	public void handleError() {
    GLenum error = glGetError();
    if (error == GL_NO_ERROR)
      return;
    closeSDL();
    throw new Exception("OpenGL error(" ~ std.string.toString(error) ~ ")");
  }
	public void closeSDL() {
    close();
    SDL_ShowCursor(SDL_ENABLE);
  }
	public  void close() {
    
  }
	public void toggleFullScreen()
	{
		if(g_videoFlags && SDL_FULLSCREEN) SDL_ShowCursor(SDL_DISABLE);
		else SDL_ShowCursor(SDL_ENABLE);
		g_videoFlags ^= SDL_FULLSCREEN;
		gScreenSurface = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, g_videoFlags);
		glMatrixMode(GL_PROJECTION);
		glFrustum(-1.0 , 1.0 , -0.75 , 0.75 , 2 , 1600);
		glViewport(0 ,0 ,width ,height);
		glClearColor(0 ,0 ,0,1.0);
//		glEnable(GL_DEPTH_TEST);
		//glEnable(GL_DEPTH_TEST);
//		glEnable(GL_BLEND);
//		glEnable(GL_LINE_SMOOTH);
//		glEnable(GL_POLYGON_SMOOTH);
		
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		if(g_videoFlags & SDL_FULLSCREEN )prefManager.prefData.recordFullScreen(true);
		else prefManager.prefData.recordFullScreen(false);
	}
	public void saveBMP(char[] name){
		SDL_SaveBMP(gScreenSurface, "screenshot.bmp");
	}
}