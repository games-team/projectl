module br.prefmanager;

private import std.stream;
private import util.record;
private import br.stage;

public class PrefManager{
	
	private:
	static const VERSION_NUM = 10;
	static const char[] PREF_FILE ="projectL.prf";
	PrefData _prefData;
	
	public this(){
		_prefData = new PrefData;
	}
	
	public void load() {
	    auto File fd = new File;
	    try {
	      int ver;
	      fd.open(PREF_FILE);
	      fd.read(ver);
	      if (ver != VERSION_NUM)
	        throw new Error("Wrong version num");
	      _prefData.load(fd);
	    } catch (Object e) {
	      _prefData.start();
	    } finally {
	      if (fd.isOpen())
	        fd.close();
	    }
 	}
	public void save(){
		auto File fd = new File;
	    fd.create(PREF_FILE);
	    fd.write(VERSION_NUM);
	    _prefData.save(fd);
	    fd.close();
	 }
	public PrefData prefData() {
    	return _prefData;
  	}
	
}

public class PrefData {
	private:
	int _fullScreen;
	int _nosound;
  	Record _record;
	public this(){
		_record = new Record(Stage.MAXSTAGE ,3);
		
	}
	
	public void start(){
		_fullScreen = 0;
		_nosound = 0;
		_record.start();
	}
	public void load(File fd) {
    _record.load(fd);
    fd.read(_nosound);
    fd.read(_fullScreen);
  }

  public void save(File fd) {
    _record.save(fd);
    fd.write(_nosound);
    fd.write(_fullScreen);
  }
  
  public void recordNoSound(bool ns){
  	if(ns)_nosound = 1;
	else _nosound = 0;
  }
  public void recordFullScreen(bool fs){
  	if(fs)_fullScreen = 1;
	else _fullScreen = 0;
  }
  public bool fullScreen(){
  	return _fullScreen != 0;

  }
  public bool nosound(){
  	return _nosound != 0;
  }
	
	public Record record(){
		return _record;
	}
}