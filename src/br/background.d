module br.background;
private import opengl;
private import std.math;
private import br.gamemanager;
private import br.ship;

public class BackGround{
	private:
	const int MAX = 10;
	int count;
	double[MAX] z;
	double[MAX] scale;
	double[MAX] rad;
	double axisX ,axisY ,axisZ;
	public:
	double theta;
	bool drawing;
	
	public this(){
		count = 0;
		for(int i=0;i<MAX;i++){
			z[i]=(screen.GAME_FAR - screen.GAME_NEAR) * (cast(double)i/cast(double)MAX) + screen.GAME_NEAR;
			scale[i] = 0.5 * (exp( 1.5 * (MAX - cast(double)i)/10.0) - 1.0) + 0.5; 
			rad[i] = (cast(double)i / MAX) * PI * 2.0;
		}
		start();
	}
	public void start(){
		axisX =1.0;axisY = 0.0; axisZ= 0.0;
		theta = 0.0;
		drawing = true;
	}
	
	public void move(){
		for(int i=0;i<MAX;i++){
//			z[i] -= 1;
			rad[i] += cast(double)i  / 360.0 * PI;//(1.0 - cast(double)(i % 2) * 2.0) / 120.0 * PI;
			if(z[i] < screen.GAME_FAR)z[i] = screen.GAME_NEAR;
		}
		count++;
	}
	public void draw(){
		
		if(drawing){
			glPushMatrix();
	//		glEnable(GL_DEPTH_TEST);
			glEnable(GL_BLEND);
	//		glEnable(GL_LINE_SMOOTH);
			
			glTranslatef(0 ,0 ,(screen.GAME_FAR - screen.GAME_NEAR)/2.0 + screen.GAME_NEAR);
			glRotated(theta ,axisX ,axisY ,axisZ);
			for(int i=0;i<MAX;i++){
				/*
				if((cast(Ship)ship).sword)glColor4f(1.0,0.7,0.5,0.05 * i);//(screen.GAME_FAR - z[i]) /(screen.GAME_FAR - screen.GAME_NEAR) * 0.6);
				else glColor4f(0.5,0.7,1.0,0.05 * i);//(screen.GAME_FAR - z[i]) /(screen.GAME_FAR - screen.GAME_NEAR) * 0.6);
				
				
				
				//glColor4f(0.0 ,0.0 ,0.0 ,1.0);
				glBegin(GL_LINE_LOOP);
				glVertex3f(cos(rad[i])*200 ,sin(rad[i])*200 ,z[i]);
				glVertex3f(-sin(rad[i])*200 ,cos(rad[i])*200 ,z[i]);
				glVertex3f(-cos(rad[i])*200 ,-sin(rad[i])*200 ,z[i]);
				glVertex3f(sin(rad[i])*200 ,-cos(rad[i])*200 ,z[i]);
				glEnd();
				*/
				
				if((cast(Ship)ship).slow)glColor4f(0.85 ,0.5 ,1.0 ,0.2 / MAX);//(screen.GAME_FAR - z[i]) /(screen.GAME_FAR - screen.GAME_NEAR) * 0.6);
				else glColor4f(0.7,1.0 ,0.5 ,0.2 / MAX);//(screen.GAME_FAR - z[i]) /(screen.GAME_FAR - screen.GAME_NEAR) * 0.6);
				
				double drawz = z[i] - ((screen.GAME_FAR - screen.GAME_NEAR)/2.0 + screen.GAME_NEAR);
				
				double a = 0.2 * cast(double)i / cast(double)MAX;
				
	//			if((cast(Ship)ship).slow)glColor3f(0.85 * a,0.5 * a ,1.0 * a);//(screen.GAME_FAR - z[i]) /(screen.GAME_FAR - screen.GAME_NEAR) * 0.6);
	//			else glColor3f(0.7 * a,1.0 * a,0.5 * a);//(screen.GAME_FAR - z[i]) /(screen.GAME_FAR - screen.GAME_NEAR) * 0.6);
				
	//			double r = 200 * scale[i];
				
				glBegin(GL_POLYGON);
				/*
				glVertex3f(cos(rad[i])*r  ,sin(rad[i])*r ,-800);
				glVertex3f(-sin(rad[i])*r ,cos(rad[i])*r ,-800);
				glVertex3f(-cos(rad[i])*r ,-sin(rad[i])*r ,-800);
				glVertex3f(sin(rad[i])*r ,-cos(rad[i])*r ,-800);
				*/
				
				glVertex3f(cos(rad[i])*200  ,sin(rad[i])*200 ,drawz);
				glVertex3f(-sin(rad[i])*200 ,cos(rad[i])*200 ,drawz);
				glVertex3f(-cos(rad[i])*200 ,-sin(rad[i])*200 ,drawz);
				glVertex3f(sin(rad[i])*200 ,-cos(rad[i])*200 ,drawz);
				
				glEnd();
			}
			glDisable(GL_BLEND);
	//		glDisable(GL_LINE_SMOOTH);
	//		glDisable(GL_DEPTH_TEST);
	/*	
			glEnable(GL_BLEND);
			if((cast(Ship)ship).sword)glColor4f(1.0 ,0.7 ,0.2 ,0.1 );
				else glColor4f(0.7 ,1.0 ,0.3 ,0.1);
			
			glBegin(GL_POLYGON);
				
			glVertex3f(screen.GAME_LEFT  ,screen.GAME_UP,-800);
	//		if((cast(Ship)ship).sword)glColor4f(1.0 ,0.7 ,0.2 ,0.01 );
	//			else glColor4f(0.7 ,1.0 ,0.3 ,0.01);
	//		glColor4f(0.0 ,0.0 ,0.0 ,0.0);
			glVertex3f(screen.GAME_LEFT  ,screen.GAME_DOWN ,-800);
			glVertex3f(screen.GAME_RIGHT ,screen.GAME_DOWN,-800);
			glVertex3f(screen.GAME_RIGHT  ,screen.GAME_UP,-800);
			
			
	//		glVertex3f(sin(rad[i])*r ,-cos(rad[i])*r ,-800);
				
			glEnd();
	*/		
			glDisable(GL_BLEND);
			glDisable(GL_LINE_SMOOTH);
			glPopMatrix();
		}
	}
	public void setAxis(double x ,double y,double z){
		double dist = sqrt(x * x + y * y + z * z);
		if((1e-6) < dist){
			axisX = x / dist;
			axisY = y / dist;
			axisZ = z / dist;
		}
	}
}