module br.bomb;
private import util.parts;
private import util.shape;
private import util.vector;
private import util.matrix;
private import util.log;
private import util.particle;
private import util.basis;

private import br.gamemanager;
private import br.blast;
private import std.math;
private import SDL_mixer;
private import br.sound;

public class Bomb:Parts{
	public this(){
		collisionManager.add(this ,collisionManager.kind.BOMB ,1);
		drawing = POLYGON;
		R = 1.0;G=1.0;B=1.0;alpha=1.0;
		shape = new SH_Sphere(1.0 ,32);
		size = 0.0;
		collisionRange = 50.0;
		pos = new Vector3(0 ,0 ,-800);
	}
	public void  move(){
		super.move();
		size += 2.0;
		collisionRange += 2.0;
	}
	public void destroy(){
		super.destroy();
	}
	public override void reportCollision(int kind){
		
	}
	public void vanish(){
 		
		super.vanish();
	}
	public bool inCollisionRange(){
		return true;
	}
}