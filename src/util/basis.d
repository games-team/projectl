module util.basis;
import std.math;

public double deglimit(double deg0){
	double deg = deg0;
	while(deg < -180)deg += 360;
	while(180 < deg)deg -= 360;
	return deg;
}
public double daimmark(double deg,double deg0){
	deg=deglimit(deg);
	deg0=deglimit(deg0);
	if(deg>deg0)
		if(deg-deg0<180)return -1.0;
		else return 1.0;
	else 
		if(deg0-deg<180)return 1.0;
		else return -1.0;
}
public double radlimit(double rad0){
	double rad = rad0;
	while(rad < -PI)rad += 2.0 * PI;
	while(PI <rad)rad -= 2.0 * PI;
	return rad;
}

public double raimmark(double rad ,double rad0){
	rad=radlimit(rad);
	rad0=radlimit(rad0);
	if(rad>rad0)
		if(rad-rad0<PI)return -1.0;
		else return 1.0;
	else 
		if(rad0-rad<PI)return 1.0;
		else return -1.0;
}