module util.log;
private import std.string;
private import std.file;
private import std.path;
private import std.cstream;
private import std.ctype;
static const char[] g_logname = "run.log";
void Log_write(char[] msg)
{
	std.cstream.dout.writeLine(msg);
	append(g_logname, msg ~ "\n");
}
void Log_write(int msg)
{
	printf("%d\n", msg);
	append(g_logname, std.string.toString(msg) ~ "\n");
}
void Log_write(Exception e)
{
	std.cstream.dout.writeLine(e.toString());
	append(g_logname, e.toString() ~ "\n");
}