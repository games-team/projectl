module util.particle;
private import opengl;
private import std.math;
private import util.parts;
private import util.vector;
private import util.shape;
private import util.matrix;
private import util.log;
private import br.gamemanager;
/*
public class Particle_old:Parts{
	public Vector3 velocity;
	public double dposeX ,dposeY ,dposeZ;
	public this(Shape shape, double size ,ubyte drawing ,Vector3 pos ,Vector3 vel ,Matrix rposeBase ,double R ,double G ,double B ,double alpha){
		this.shape = cast(Shape)shape.clone();
		this.size = size;
		this.pos = cast(Vector3)pos.clone();
		this.velocity = cast(Vector3)vel.clone();
		setPoseBase(rposeBase);
		this.R = R;
		this.G = G;
		this.B = B;
		this.alpha = alpha;
		this.drawing = drawing;
		dposeX = rand.nextFloat(10);
		dposeY = rand.nextFloat(10);
		dposeZ = rand.nextFloat(10);
	}
	public void move(){
		super.move();
		if(cnt > 3){
			pos += velocity;
			poseX += dposeX;
			poseY += dposeY;
			poseZ += dposeZ;
			
			velocity *= 0.95;
		}
		alpha *= 0.95;
		
		if(alpha < 0.1)vanish();
		
	}

}
*/
public class Particle{
	public:
	Vector3 velocity;
	double dpose;
	double pose;
//	double dposeX ,dposeY ,dposeZ;
	double poseX ,poseY ,poseZ;
	Matrix poseBase;
	
	Shape shape;
	double size;
	ubyte drawing;
	Vector3 pos;
//	Vector3 vel;
//	Matrix rposeBase;
	double R ,G ,B ,alpha;
	int cnt;
	
	private bool _exists;
	
	public this(){
		_exists = false;
	}
	public void set(Shape shape, double size ,ubyte drawing ,Vector3 pos ,Vector3 vel ,Matrix rposeBase ,double R ,double G ,double B ,double alpha){
		this.shape = cast(Shape)shape.clone();
		this.size = size;
		this.pos = cast(Vector3)pos.clone();
		this.velocity = cast(Vector3)vel.clone();
		this.poseBase = cast(Matrix)rposeBase.clone();
		this.R = R;
		this.G = G;
		this.B = B;
		this.alpha = alpha;
		this.drawing = drawing;
		poseX = 0.0; poseY = 0.0; poseZ = 0.0;
		
		pose = 0.0;
		dpose = rand.nextFloat(20);
		poseX = rand.nextFloat(1);
		poseY = rand.nextFloat(1);
		poseZ = rand.nextFloat(1);
		
		
		double dist = sqrt(poseX * poseX + poseY * poseY + poseZ * poseZ);
		if((1e-6) < dist){
			poseX = poseX / dist;
			poseY = poseY / dist;
			poseZ = poseZ / dist;
		}
		/*
		dposeX = rand.nextFloat(10);
		dposeY = rand.nextFloat(10);
		dposeZ = rand.nextFloat(10);
		*/
		cnt = 0;
		_exists = true;
	}
	public void move(){
//		super.move();
		cnt ++;
		if(cnt > 3){
			pos += velocity;
			
			pose += dpose;
			/*
			poseX += dposeX;
			poseY += dposeY;
			poseZ += dposeZ;
			*/
			velocity *= 0.95;
		}
		alpha *= 0.95;
		
		if(alpha < 0.1)vanish();
		
	}
	public void draw(){
		
		
		//glLoadIdentity();
//		double size = 10.0;
//		Matrix rpose = poseBase;
		
		//		Matrix drawpose = rpose
		//Matrix p = matRotateX(rposeX) * matRotateY(rposeY) * matRotateZ(rposeZ) * matScale(size * scaleX ,size * scaleY ,size * scaleZ) * poseBase;
		
		glTranslatef(pos.x ,pos.y ,pos.z);
		glScaled(size ,size ,size);
		glRotated(pose ,poseX ,poseY ,poseZ);
		
		if(shape !is null && (1e-6) < size){
		
//			shape.transformMatrix(rpose);
			
			Vector3[] av = shape.v;
			

			
//			glEnable(GL_POLYGON_SMOOTH);
			
			Vector3 ave;
			if(drawing & Parts.POLYGON){
				polyRect[] pr = shape.rects;
				for(int i = 0; i < pr.length ;i ++){
					
					glBegin(GL_POLYGON);
						ave = new Vector3((av[pr[i].v[0]] + av[pr[i].v[1]] + av[pr[i].v[2]] + av[pr[i].v[3]]) / 4.0);
						double a = fmax(0.0,(1.0 + (pos.z + ave.z) / 1600.0) * alpha);
						glColor3d(R * a ,G * a ,B * a);
					//glColor4d(R ,G ,B ,fmax(0.0,(1.0 + (pos.z + ave.z) / 1600.0) * alpha)); //glColor4f(1.0f ,1.0f ,1.0f,(1.0 + (pos.z + ave.z) / 1600.0) * 0.6);
					for(int j = 0;j < 4 ;j ++){
						glVertex3f(av[pr[i].v[j]].x,av[pr[i].v[j]].y,av[pr[i].v[j]].z);
					}
					
					glEnd();
				}
			}
			
//			glDisable(GL_POLYGON_SMOOTH);
			glEnable(GL_BLEND);
			glEnable(GL_LINE_SMOOTH);
			if(drawing & Parts.WIRE){
				
				
				int[][] wires = shape.wires;
				
//				double a = fmin(1.0 ,fmax(0.0 ,alpha));
				for(int i = 0; i < wires.length ;i ++){
//					glColor3d(R * a ,G * a ,B * a);
//					glColor4d(R ,G ,B ,fmax(0.0,alpha));
					
					
					

					glBegin(GL_LINE_STRIP);
					
					glVertex3f(0 ,0,0);
					glVertex3f(0 ,0,0);
					
					
					glColor4d(R ,G ,B ,fmax(0.0,alpha));
					
		   			for(int j = 0; j < wires[i].length;j ++){
//						double a = alpha * fmax(0.0,(1.0 + (pos.z + av[wires[i][j]].z + 600.0) / 1200.0));
//						glColor4d(R ,G ,B ,fmax(0.0,a));

	 	  	 			glVertex3f(av[wires[i][j]].x ,av[wires[i][j]].y ,av[wires[i][j]].z);


						//glVertex3f(av[wires[i][j + 1]].x ,av[wires[i][j + 1]].y ,av[wires[i][j + 1]].z);
	  	  			}
					
					
					
					glEnd();
					
					
				
				}
				
			}
			
			
			
			glDisable(GL_LINE_SMOOTH);
			glDisable(GL_BLEND);
			
//			shape.resetVertex();
			
			
		}
		
		
//		drawn = true;
	}
	public void vanish(){
		_exists = false;
	}
	public bool exists(){
		return _exists;
	}

}

public void makeParticle(Shape shape, double size, ubyte drawing, Vector3 pos ,Vector3 vel ,Matrix rpose ,double R ,double G ,double B ,double alpha){
	if(shape !is null){
		foreach(polyRect r;shape.rects){
			Vector3[] v;
			int[][] wires;
			polyRect[] rects;
			v.length = r.v.length;
			for(int i=0;i<v.length;i++){v[i]=shape.v[r.v[i]];}
			wires.length = 1;
			wires[0].length = r.v.length + 1;
			for(int i=0;i<wires[0].length;i++){
				if(i == wires[0].length-1)wires[0][i]=0;
				else wires[0][i]=i;
			}
			rects.length = 1;
			for(int i=0;i<rects[0].v.length;i++){rects[0].v[i]=i;}
			
			Vector3 average = new Vector3();
			
			foreach(Vector3 vec;v){
				average += vec;
			}
			
			average = average / cast(double)v.length;
			
			foreach(inout Vector3 vec;v){
				vec = vec - average;
			}
			
			Vector3 rpos = pos + vec3translate(average , rpose);
			
			Shape s = new Shape(v ,wires ,rects);
			Vector3 velocity = vec3Normalize(average) * 5.0 + vel;
			
//			new Particle_old(s ,1.0 ,drawing ,rpos ,velocity ,rpose  ,R ,G ,B ,alpha);
			Particle p = particleManager.getInstance;
			if(p !is null)p.set(s ,size ,drawing ,rpos ,velocity ,rpose ,R ,G ,B ,alpha);
			//new Particle(s ,size ,drawing ,rpos ,velocity ,poseBase ,poseX ,poseY ,poseZ ,R ,G ,B ,alpha);
		}
	}
}
private Shape simpleParticleShape;
public void makeSimpleParticle(int num ,double size, ubyte drawing, Vector3 pos ,double R ,double G ,double B ,double alpha){
	if(simpleParticleShape is null){
		Vector3[] v;
		int[][] wires;
		polyRect[] rects;
		v.length = 4;
		v[0] = new Vector3(0.7 ,0.0 ,0.0);
		v[1] = new Vector3(0.7 ,-0.0 ,0.0);
		v[2] = new Vector3(-0.7 ,-0.5 ,0.0);
		v[3] = new Vector3(-0.7 ,0.5 ,0.0);
		
		wires.length = 1;
		wires[0].length = 5;
		wires[0][0] = 0;wires[0][1] = 1;wires[0][2] = 2;wires[0][3] = 3;wires[0][4] =0;
		
		rects.length = 1;
		rects[0].v[0]=0;rects[0].v[1]=1;rects[0].v[2]=2;rects[0].v[3]=3;
		simpleParticleShape = new Shape(v ,wires ,rects);
	}
	Shape s = cast(Shape)simpleParticleShape.clone();
	for(int i=0;i<num;i++){
		
		Particle p = particleManager.getInstance;
		if(p !is null){
			Vector3 velocity = new Vector3(4.0 - rand.nextFloat(8.0) ,4.0 - rand.nextFloat(8.0) ,4.0 - rand.nextFloat(8.0) );
			double theta ,phi;
			theta = rand.nextFloat(360);
			phi = rand.nextFloat(180);
			p.set(s ,size ,drawing ,pos ,velocity ,matRotateX(theta)*matRotateY(phi) ,R ,G ,B ,alpha);
		}
	}
	
}

ParticleManager particleManager;

public class ParticleManager{
		public:
	Particle[] particles;
	//List!(Parts) parts;
	protected:
	//List!(int) van = new List!(int)();
  int particleIdx = 0;
	const int maxParticle;
	public this() {
		maxParticle = 16;
		particles.length = maxParticle;
		foreach(inout Particle p;particles){
			p = new Particle();
		}
		//parts = new List!(Parts)();
		particleIdx = 0;
	}

  public this(int n) {
		maxParticle = n;
		particles.length = maxParticle;
		foreach(inout Particle p;particles){
			p = new Particle();
		}
		//parts = new List!(Parts)();
		particleIdx = 0;
  }

	public Particle getInstance(){
		
		for(int i = 0;i < particles.length;i ++){
			particleIdx ++;
			if(particles.length <= particleIdx)particleIdx = 0;
			if(!particles[particleIdx].exists()){
				return particles[particleIdx];
			}
			
		}
		
		return null;
		
		//if(maxParts <= parts.size)return false;
		//parts.push_back(p);
		
	}
	public void run() {
		
		for (int i = 0;i < particles.length;i ++){
			Particle an = particles[i];
			
    	  	if (an.exists){
				
        		an.move();
    	  	}
			
    	}
		  

   }
		
		
  public void draw() {
		
		for (int i = 0;i < particles.length;i ++){
			Particle an = particles[i];
			if (an.exists){
				glPushMatrix();
       			an.draw();
				glPopMatrix();
			}
		}
  }


  public void clear() {
		foreach(inout Particle a;particles){
			if(a !is null && a.exists){
				a.vanish();
			}
		}
   	//parts.length = 0;
    particleIdx = 0;
  }
}