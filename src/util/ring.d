module util.ring;
private import opengl;
private import std.math; 
private import util.parts;
private import util.vector;
private import util.matrix;
private import br.gamemanager;

public interface Caller{
	public void reportRing(Ring ring);
	public bool exists();
}
public class Ring:Parts{
	private:
	static const int DIVISION = 16;
	static double[] si;
	static double[] co;
	Caller caller;
	char[] _name;
	int span;
	real sint ,cost;
	public this(Caller caller ,Vector3 pos ,char[] name ,int span ,double R ,double G,double B,double alpha ,real deg = 90.0){
		if(caller is null || !caller.exists){
			vanish();
			return;
		}
		if(si.length == 0){
			si.length = DIVISION;
			for(int i=0;i<DIVISION;i++){
				si[i] = sin(PI *2.0 * cast(double)i / DIVISION);
			}
		}
		if(co.length == 0){
			co.length = DIVISION;
			for(int i=0;i<DIVISION;i++){
				co[i] = cos(PI *2.0 * cast(double)i / DIVISION);
			}
		}
		this.pos = pos;
		/*
		foreach(inout Vector3 lpos;locuspos){
			lpos = cast(Vector3)pos.clone();
		}
		*/
		this.caller = caller;
		this._name = name.dup;
		this.span = span;
		this.R=R;
		this.G=G;
		this.B=B;
		this.alpha=alpha;
		
		deg = 0.0;
		sint = sin(deg * PI / 180.0);
		cost = cos(deg * PI / 180.0);
		
		
		ringManager.add(this);
	}
	public void move(){
		super.move();
		if(caller !is null && caller.exists){
			if(span <= cnt)report();
		}else vanish();
		/*
		if(pos.y < screen.GAME_DOWN || screen.GAME_UP < pos.y ||
			screen.GAME_RIGHT  < pos.x || pos.x < screen.GAME_LEFT )vanish();
		*/
	}
	public void report(){
		if(caller !is null && caller.exists == true){
			caller.reportRing(this);
		}
		vanish();
	}
	public void draw(){
//		super.draw();
		glTranslatef(pos.x ,pos.y ,pos.z);
		if(caller is null)return;
		glEnable(GL_BLEND);
		double radius1=fmax(0.0,cast(double)(span - cnt + 9) * 3);
		double radius0=fmax(0.0,cast(double)(span - cnt - 0)  * 3);
		for(int j=0;j<DIVISION-1;j++){
			glBegin(GL_POLYGON);
//			glColor3f(0 ,0 ,0);
			glColor4f(R,G,B,0.0);
			glVertex3f(radius1*co[j]*cost,radius1*si[j]*cost ,radius1*sint);
			glVertex3f(radius1*co[j+1]*cost,radius1*si[j+1]*cost ,radius1*sint);
//			glColor3f(R * alpha,G * alpha ,B * alpha);
			glColor4f(R,G,B,alpha);
			glVertex3f(radius0*co[j+1]*cost,radius0*si[j+1]*cost ,radius0*sint);
			glVertex3f(radius0*co[j]*cost,radius0*si[j]*cost ,radius0*sint);
			
			glEnd();
		}
		glBegin(GL_POLYGON);
//		glColor3f(0 ,0 ,0);
		glColor4f(R,G,B,0);
		glVertex3f(radius1*co[0]*cost,radius1*si[0]*cost ,radius1*sint);
		glVertex3f(radius1*co[DIVISION-1]*cost,radius1*si[DIVISION-1]*cost ,radius1*sint);
//		glColor3f(R * alpha,G * alpha ,B * alpha);
		glColor4f(R,G,B,alpha);
		glVertex3f(radius0*co[DIVISION-1]*cost,radius0*si[DIVISION-1]*cost ,radius0*sint);
		glVertex3f(radius0*co[0]*cost,radius0*si[0]*cost ,radius0*sint);
		glEnd();
		glDisable(GL_BLEND);

	}
	public char[] name(){
		return _name;
	}
}