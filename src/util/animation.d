module util.animation;
private import util.parts;
private import std.math;
public abstract class Animation{
	public:
	int count;
	int span;
	Parts parts;
	protected:
	bool _exists;
	bool repeat;
	bool _active;
	public this(int span, bool repeat = false){
		//animationManager.add(this);
		this.span = span;
		this.repeat = repeat;
		count = 0;
		_exists = true;
		_active = true;
	}
	public void set(Parts parts){
		this.parts = parts;
	}
	public void run(){
		count ++;
		if(span <= count){
			if(repeat)count = 0;
			else{
				vanish();
				return;
			}
		}
	}
	public void vanish(){
		_exists = false;
		_active = false;
		
	}
	public void stop(){
		_active = false;
	}
	public void resume(){
		_active = true;
	}
	public bool active(){
		return _active;
	}
	public bool exists(){
		return _exists;
	}
}
/*
public AnimationManager animationManager;
public class AnimationManager{
	public:
	Animation[] animes;
	//List!(Parts) parts;
	protected:
	//List!(int) van = new List!(int)();
  int animeIdx = 0;
	const int maxAnimation;
	public this() {
		maxAnimation = 16;
		animes.length = maxAnimation;
		//parts = new List!(Parts)();
		animeIdx = 0;
	}

  public this(int n) {
		maxAnimation = n;
		animes.length = maxAnimation;
		//parts = new List!(Parts)();
		animeIdx = 0;
  }

	public bool add(Animation a){
		for(int i = 0;i < animes.length;i ++){
			if(animes[animeIdx] is null || !animes[animeIdx].exists()){
				animes[animeIdx] = a;
				return true;
			}
			animeIdx ++;
			if(animes.length <= animeIdx)animeIdx = 0;
		}
		return false;
		//if(maxParts <= parts.size)return false;
		//parts.push_back(p);
		
	}
	public void run() {
    //List!(int) van = new List!(int)();
		
		for (int i = 0;i < animes.length;i ++){
			Animation an = animes[i];
      if (an !is null && an.exists && an.active){
				
        an.run();
				if(an.exists == false)an = null;
      }else{
				//van.push_back(i);
      }
    }
		
		
  }


  public void clear() {
		foreach(inout Animation a;animes){
			if(a !is null && a.exists){
				a.vanish();
			}
		}
   	//parts.length = 0;
    animeIdx = 0;
  }
}
*/
