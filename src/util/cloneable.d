module util.cloneable;
interface Cloneable{
	public Object clone();
}